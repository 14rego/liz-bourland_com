<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title></title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="layout.css" rel="stylesheet" type="text/css" />
</head>

<body id="page1">
	<div class="tall_top">
		<div id="main">
			<div id="left_side">
				<div class="indent">
					<img class="title" alt="" src="images/1_t1.gif" />
					<div class="line_hor">
						<div class="block1">
							<div class="ind">
								<h3>Objective</h3>
                                                Obtain a managerial position where I can maximize my supervisory skills, qualityassurance, and training experience.
							</div>
						</div>
					</div>
					<div class="line_hor">
						<div class="block1">
							<div class="ind">
								<h3>Contact Me</h3>
                                           Olathe, KS 66062<br />
                                           <a href="EBourland_2014MAR.pdf" target="_blank">Download 2014 R&eacute;sum&eacute;</a>
							</div>
						</div>
					</div>
					<div class="line_hor">
						<div class="block1">
							<div class="ind">
								<h3>Education</h3>
                                                Mid-America Nazarene University,<br />
                                                Olathe, Kansas<br />
                                                <ul>
                                                	<li>Aug 1998 - May 2004</li>
                                                	<li><i>99 credits received</i><br />
									Classes related to Psychology<br />and Criminal Justice</li>
									<li><i>Affiliations</i><br />
									8/1998 to 1/2000: Women's basketball</li>
                                                </ul>
							</div>
						</div>
					</div>
					<!--div class="line_hor">
						<div class="block1">
							<div class="ind">
								<h3>References</h3>
                                                Personal and professional references are available upon request
							</div>
						</div>
					</div-->
					<div class="line_hor">
						<div class="block1">
							<div class="ind">
                                        <h3>Additional Skills</h3>
                                         Computer Literate
                                         <div style="font-weight:normal;">Expert, 15 years of experience</div>
                                         Microsoft Office
                                         <div style="font-weight:normal;">Proficient, 10 years of experience</div>
							</div>
						</div>
					</div>
					<div class="line_hor">
						<div class="block1">
							<div class="ind">
                                    <h3>Additional Info</h3>
                                     <ul>
								<li>PTCB certified</li>
								<li>Excellent communication skills</li>
								<li>Good with customers and customer service</li>
								<li>Proficient in Microsoft Outlook, Excel, RxClaim</li>								<li>Familiar with Six Sigma concepts</li>
                                     </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="right_side">
				<div class="wrapper">
					<div id="header">
						<div class="row_1">
							<div class="fleft">
								<a href="#"><img alt="" src="images/logo.gif" /></a>
							</div>
							<div class="fright">
								<div class="indent">
									<a href="index.html" style="visibility:hidden;"><img alt="" src="images/header_link1.gif"/></a><a href="#" style="visibility:hidden;"><img alt="" src="images/header_link2.gif" /></a>
								</div>
							</div>
						</div>
						<div class="row_2">
							<div class="left">
								<div class="right">
									<div class="indent">
										<!--div style="color:#fff; font-size:50px; line-height:50px; font-weight:bold;">Elizabeth Bourland</div-->
                                                            <div style="position:relative; top:-18px;"><img alt="Elizabeth Bourland" src="images/liz.png" border="0"/></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="content">
						<div class="row_1" style="width:100%;">
							<div class="indent">
								<div class="col_1">
									<div class="indent">
										<div class="block">
											<div class="l_t">
												<div class="r_t">
													<div class="r_b">
														<div class="l_b">
															<div class="ind">
																<div class="link2">
																	Pharmacy Technician Supervisor <br><span style="font-style:italic; font-weight:normal; color:#7d7d7d;">Jul 2012 &mdash; Mar 2014</span>
																</div>
																<div class="line_hor1">
																	Specialty Pharmacy,<br />
                                                                                     CVS Caremark,<br />
																	Lenexa, Kansas
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Job Functions</div>
																	<ul>
                                                                                                      	<li>Manage team of 15 technicians</li>
                                                                                                      	<li>Work closely with techs and pharmacists to ensure specialty medications are shipped accurately and in a timely manner</li>
                                                                                                      	<li>Work with Human Resources when appropriate regarding employees’ behavior, job performance, etc.</li>
                                                                                                      	<li>Hire new employees and terminate as necessary</li>
                                                                                                      	<li>Provide employee evaluations and production incentives quarterly</li>
                                                                                                      	<li>Update business models and policies and procedures as business needs dictate</li>
                                                                                                      </ul>
																	<br /><a class="link4" href="http://www.cvscaremarkspecialtyrx.com/">more about CVS Caremark</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
                                                            <br />
										<div class="block">
											<div class="l_t">
												<div class="r_t">
													<div class="r_b">
														<div class="l_b">
															<div class="ind">
																<div class="link2">
																	Pharmacy Technician Supervisor <br><span style="font-style:italic; font-weight:normal; color:#7d7d7d;">Apr 2009 &mdash; Jul 2012</span>
																</div>
																<div class="line_hor1">
																	Mail Service<br />
																	Prescription Solutions,<br />
																	Overland Park, Kansas
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Job Functions</div>
																	<ul>
                                                                                                      	<li>Manage team of 16-24 technicians and one lead technician</li>
                                                                                                      	<li>Work closely with techs and pharmacists to ensure prescriptions are shipped accurately and in a timely manner</li>
                                                                                                      	<li>Work with Human Resources when appropriate regarding employees' behavior, job performance, etc&hellip;</li>
                                                                                                      	<li>Hire new employees and terminate as necessary</li>
                                                                                                      </ul>
																	<br /><a class="link4" href="https://www.prescriptionsolutions.com">more about Prescription Solutions</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
                                                            <br />
										<div class="block">
											<div class="l_t">
												<div class="r_t">
													<div class="r_b">
														<div class="l_b">
															<div class="ind">
																<div class="link2">
																	Pharmacy Technician <br><span style="font-style:italic; font-weight:normal; color:#7d7d7d;">Jan 2006 &mdash; Feb 2008</span>
																</div>
																<div class="line_hor1">
																	Customer Service<br />
																	Prescription Solutions,<br />
																	Overland Park, Kansas
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Job Functions</div>
																	<ul>
                                                                                                      	<li>Fill new prescriptions mailed in by patients</li>
                                                                                                      	<li>Fill new prescriptions faxed in by doctor's offices</li>
                                                                                                      	<li>Contact patients regarding changes to their medications</li>
                                                                                                      	<li>Contact doctor's offices for new or refill medications</li>
                                                                                                      	<li>Work alongside pharmacists on certain projects</li>
                                                                                                      </ul>
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Achievements</div>
																	<ul>
                                                                                                      	<li>Ranked 4th out of 70 technicians in script accuracy and efficiency</li>
                                                                                                      	<li>Multiple attendance awards</li>
                                                                                                      	<li>Cross-trained in Data Entry</li>
                                                                                                      	<li>Entrusted by supervisor to come in and work unsupervised</li>
                                                                                                      	<li>Acted as a "go-to" technician, same as a lead technician on the customer service side</li>
                                                                                                      	<li>Assisted in numerous training classes</li>
                                                                                                      </ul>
																	<br /><a class="link4" href="https://www.prescriptionsolutions.com">more about Prescription Solutions</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<br /><br />
                                                </div>
                                                
								<div class="col_1">
									<div class="indent">
										<div class="block">
											<div class="l_t">
												<div class="r_t">
													<div class="r_b">
														<div class="l_b">
															<div class="ind">
																<div class="link2">
																	Lead Pharmacy Technician <br><span style="font-style:italic; font-weight:normal; color:#7d7d7d;">Feb 2008 &mdash; Apr 2009</span>
																</div>
																<div class="line_hor1">
																	Mail Service<br />
																	Prescription Solutions,<br />
																	Overland Park, Kansas<br />
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Job Functions</div>
																	<ul>
                                                                                                      	<li>Act as liaison between technicians and supervisor</li>
                                                                                                      	<li>Input daily numbers into Excel spreadsheet</li>
                                                                                                      	<li>Monitor attendance</li>
                                                                                                      	<li>Monitor queue movement</li>
                                                                                                      	<li>Take supervisor calls if necessary</li>
                                                                                                      	<li>Help with difficult orders when called upon</li>
                                                                                                      	<li>Work closely with other leads, pharmacists, and supervisors</li>
                                                                                                      	<li>Assist in interviews when necessary</li>
                                                                                                      </ul>
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Achievements</div>
																	<ul>
                                                                                                      	<li>Received PTCB certification in June 2008</li>
                                                                                                      	<li>Cross trained in CHP, PIA, DEX and PSC queues</li>
                                                                                                      	<li>Developed close relationships with technicians and pharmacists</li>
                                                                                                      	<li>Worked closely with members of upper management</li>
                                                                                                      	<li>Accepted as trainer in upcoming IRIS classes</li>
                                                                                                      </ul>
																	<br /><a class="link4" href="https://www.prescriptionsolutions.com">more about Prescription Solutions</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
                                                            <br />
										<div class="block">
											<div class="l_t">
												<div class="r_t">
													<div class="r_b">
														<div class="l_b">
															<div class="ind">
																<div class="link2">
																	Pharmacy Technician <br><span style="font-style:italic; font-weight:normal; color:#7d7d7d;">Nov 2004 &mdash; Jan 2006</span>
																</div>
																<div class="line_hor1">
																	Bond Pharmacy,<br />
																	Kansas City, Kansas<br />
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Job Functions</div>
																	<ul>
                                                                                                      	<li>Fill prescriptions</li>
                                                                                                      	<li>Take refills from nurses over telephone</li>
                                                                                                      	<li>Update inventory</li>
                                                                                                      	<li>Customer service</li>
                                                                                                      </ul>
																	<br /><a class="link4" href="http://www.goodneighborpharmacy.com/">more about Bond Pharmacy</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
                                                  <br />
										<div class="block">
											<div class="l_t">
												<div class="r_t">
													<div class="r_b">
														<div class="l_b">
															<div class="ind">
																<div class="link2">
																	Pharmacy Technician <br><span style="font-style:italic; font-weight:normal; color:#7d7d7d;">Feb 1999 &mdash; Sep 2004</span>
																</div>
																<div class="line_hor1">
																	Price Chopper Pharmacy,<br />
																	Olathe, Kansas
                                                                                                      <div style="padding-top:10px; font-weight:normal; font-style:italic;">Job Functions</div>
																	<ul>
                                                                                                      	<li>Fill prescriptions brought in by customers</li>
                                                                                                      	<li>Assist pharmacist with tasks</li>
                                                                                                      	<li>Answer phones</li>
                                                                                                      	<li>Attend to customers</li>
                                                                                                      	<li>Run cash register</li>
                                                                                                      	<li>Order specific medications from wholesaler</li>
                                                                                                      </ul>
																	<br /><a class="link4" href="http://www.mypricechopper.com/store_info/store_detail.aspx?locationId=67">more about Price Chopper</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div id="footer">
			<div class="fleft">
				<div class="indent">
					Elizabeth Bourland &copy; <?php echo date('Y'); ?>
				</div>
			</div>
			<div class="fright">
				<div class="indent">
					<span><strong>E-mail:</strong> <a href="mailto:liz@liz-bourland.com">liz@liz-bourland.com</a></span>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>